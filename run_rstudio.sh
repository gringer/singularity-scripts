#!/bin/bash
#
# Loosely based on https://www.rocker-project.org/use/singularity/
#
# TODO:
#  - Set the LC_* environment variables to suppress warnings in Rstudio console
#  - Use an actually writable common Singularity cache somewhere to share images between users
#    (current setup has permissions issue).
#  - Determine why laptop -> login-node -> compute-node SSH forwarding isn't working (sshd_config ?)
#  - Allow srun/sbatch from within the container.
#
# Update by David Eccles 2024-May-30
#  - Trimming out to make it specific to our rstudio Singularity image

## Run like ./run_rstudio.sh <port>

#set -o xtrace

# We use this modified version of rocker/rstudio by default, with Seurat and required
# dependencies already installed.
IMAGE=${IMAGE:-"/mnt/md0/deccles/singularityImages/rstudio.simg"}

PORT=${RSTUDIO_PORT:-$1}
PORT=${PORT:-8603}
echo "Proposing to start server on port ${PORT}"
export PASSWORD=$(openssl rand -base64 15)
export SINGULARITY_CACHEDIR=${SINGULARITY_CACHEDIR:-"/mnt/md0/deccles/.singularity/cache"}

function get_port {
    # lsof doesn't return open ports for system services, so we use netstat
    until ! ss -tulwn | grep LISTEN | grep  ":${PORT}";
    do
        ((PORT++))
        echo "Checking port: ${PORT}"
    done
    echo "Got one !"
}

IMAGE_SLASHED=$(echo $IMAGE | sed 's/:/\//g')
RSTUDIO_HOME=${HOME}/.rstudio-rocker/${IMAGE_SLASHED}/session
RSTUDIO_TMP=${HOME}/.rstudio-rocker/${IMAGE_SLASHED}/tmp
R_LIBS_USER=${HOME}/.rstudio-rocker/${IMAGE_SLASHED}
mkdir -p ${RSTUDIO_HOME}
mkdir -p ${R_LIBS_USER}
mkdir -p ${RSTUDIO_TMP}
mkdir -p ${RSTUDIO_TMP}/var/run

echo
echo "Finding an available port ..."
get_port

LOCALPORT=${PORT}

echo "Point your web browser at http://10.0.0.16:${LOCALPORT}"
echo
echo "[user authentication is disabled]"
#echo "Login to RStudio with:"
#echo "  username: ${USER}"
#echo "  password: ${PASSWORD}"
echo
echo "Starting RStudio Server (R version from image ${IMAGE})"

# Set some locales to suppress warnings
LC_CTYPE="C"
LC_TIME="C"
LC_MONETARY="C"
LC_PAPER="C"
LC_MEASUREMENT="C"

SINGULARITYENV_PASSWORD="${PASSWORD}" \
singularity exec \
                 --bind ${HOME}:/home/rstudio-server \
                 --bind ${RSTUDIO_HOME}:${HOME}/.rstudio \
                 --bind ${R_LIBS_USER}:${R_LIBS_USER} \
                 --bind ${RSTUDIO_TMP}:/tmp \
                 --bind=${RSTUDIO_TMP}/var:/var/lib/rstudio-server \
                 --bind=${RSTUDIO_TMP}/var/run:/var/run/rstudio-server \
                 --env R_LIBS_USER=${R_LIBS_USER} \
                 ${IMAGE} \
                 /usr/lib/rstudio-server/bin/rserver --auth-none=1 --www-port=${PORT} --server-user=${USER}

printf 'rserver exited' 1>&2
