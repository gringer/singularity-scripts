#!/bin/sh

## To automate running this on server reboot, add the following line to 'crontab -e':
## @reboot /mnt/md0/deccles/singularityFiles/startInstances.sh

cd /mnt/md0/deccles/singularityFiles/

singularity instance start --writable --fakeroot --bind /stornext/DataSAN/Bioinformatics/R_data/WebService/:/share ../singularityImages/web-server.simg web-server
singularity instance start --writable --fakeroot ../singularityImages/cellkeycap.simg cellkeycap
singularity instance start --fakeroot --writable -B /stornext/DataSAN/Bioinformatics/R_data/RStudioProjects/Seurat/logs:/app/logs,/stornext/DataSAN/Bioinformatics/R_data/RStudioProjects/Seurat/:/app/data /mnt/md0/deccles/singularityImages/shiny-cell-browser.simg shiny-cell-browser
singularity instance start --writable --fakeroot -B /stornext/DataSAN/Bioinformatics/server_config/JBrowse2:/app/jbrowse2 ../singularityImages/jbrowse2.simg jbrowse2
singularity instance start --writable --fakeroot -B /stornext/DataSAN/Bioinformatics/JBrowse-FR:/app/jbrowse1 ../singularityImages/jbrowse1.simg jbrowse1
singularity instance start --writable --fakeroot /mnt/md0/deccles/singularityImages/room-labeller.simg room-labeller
#singularity instance start --writable --fakeroot -B /stornext/DataSAN/Bioinformatics/R_data/ShinyApps/MM_Th2_DC_RNAseq:/app  --net --network-args "portmap=3801:4242/tcp" /mnt/md0/deccles/singularityImages/shiny-cell-browser.simg shiny-server

## Demonstration of port mapping
#singularity instance start --writable --fakeroot --net --network-args "portmap=8081:8080/tcp" --bind /stornext/DataSAN/Bioinformatics/JBrowse-MB:/share ../singularityImages/web-server.simg web-server-jbrowse
