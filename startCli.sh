#!/bin/sh

singularity shell \
  --bind /stornext/DataSAN/Bioinformatics/intermediate_results/:/intermediate_results \
  --bind /stornext/DataSAN/Bioinformatics/Scratch:/Scratch \
  --bind /stornext/DataSAN/Bioinformatics/R_data:/R_data \
  --bind /mnt/md0/deccles/tmp:/data_tmp \
  --bind /mnt/md0/deccles:/md0 \
  ../singularityImages/david-cli.simg
