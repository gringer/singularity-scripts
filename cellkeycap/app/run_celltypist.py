#!/usr/bin/env python3
import scanpy as sc
import celltypist
from celltypist import models
import os

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--filename", help='Path to the input count matrix')
parser.add_argument("--model", help='Model used to predict the input cells')
parser.add_argument("--transpose_input", help='Whether to transpose the input matrix')
parser.add_argument("--mode", help='The way cell prediction is performed')
parser.add_argument("--p_thres", help='Probability threshold for the multi-label classification')
parser.add_argument("--majority_voting", help='Whether to refine the predicted labels by running the majority voting classifier after over-clustering')
parser.add_argument("--min_prop", help='Minimum proportion of cells required to support naming of the subcluster')
args = parser.parse_args()

print("Current directory: " + os.getcwd())

print("Command line arguments: ", args.filename, args.model, args.transpose_input, args.mode, args.p_thres, args.majority_voting, args.min_prop)

# Update models (if needed)
model = models.Model.load(model = args.model)
input_file = args.filename
model_name = args.model
#predictions = celltypist.annotate(input_file, transpose_input = args.transpose_input, model = model_name, mode = args.mode,
#                                  p_thres=args.p_thres, majority_voting=args.majority_voting, min_prop=args.min_prop)
predictions = celltypist.annotate(input_file, transpose_input = args.transpose_input, model = model_name, mode = args.mode, majority_voting=args.majority_voting)
predictions.to_table(folder = 'uploads', prefix = 'CT_predictions_')

print("Finished!")
