from flask import Flask, flash, render_template, request, redirect, url_for, send_from_directory
import os
import subprocess

UPLOAD_FOLDER = '/usr/src/app/uploads'
ALLOWED_EXTENSIONS = ['.csv', '.csv.gz']

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = 'Wae8reuvoa1igi7'

@app.route("/", methods=['GET', 'POST'])

def upload_file():
    if request.method == 'POST':
       if 'filename' not in request.files:
          flash('No file part')
          return redirect(request.url)
       file = request.files['filename']
       if file.filename == '':
          flash('No selected file')
          return redirect(request.url)
       ### TODO: dangerous - this deletes the contents of the upload folder
       #commandLine = list(('rm', os.path.join(app.config['UPLOAD_FOLDER'], "*")))
       #runProcess = subprocess.call(commandLine)
       if file:
          if file.filename.endswith('.csv'):
             filename = 'celltypist_input.csv'
             fullFileName = os.path.join(app.config['UPLOAD_FOLDER'], filename)
             file.save(fullFileName)
          elif file.filename.endswith('.csv.gz'):
             filename = 'celltypist_input.csv.gz'
             fullFileName = os.path.join(app.config['UPLOAD_FOLDER'], filename)
             file.save(fullFileName)
             commandLine = list(('bgzip', '-d', '-@', '12', fullFileName))
             runProcess = subprocess.call(commandLine)
             fullFileName = filename.rsplit( ".", 1 )[ 0 ] # filename.rsplit( ".", 1 )[ 0 ]
          else:
             flash('Invalid file extension (should be .csv or .csv.gz)')
             return redirect(request.url)
          commandLine = list(('python',
                              'run_celltypist.py',
                              '--filename', fullFileName,
                              '--model', request.form['model'],
                              '--transpose_input', 'True' if request.form.get('transpose_input') else 'False',
                              '--mode', request.form['mode'],
                              '--p_thres', request.form['p_thres'],
                              '--majority_voting', 'True' if request.form.get('majority_voting') else 'False',
                              '--min_prop', request.form['min_prop']))
          runProcess = subprocess.call(commandLine)
          return send_from_directory(app.config['UPLOAD_FOLDER'], path='CT_predictions_predicted_labels.csv',
                                     as_attachment=True, mimetype='text/csv')
    return render_template('upload.html')

def hello(name=None):
    return render_template('index.html', name=name)

def index():
  return render_template('index.html', name=None)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=4496)
